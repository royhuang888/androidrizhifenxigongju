#ifndef TAGANALYSE_H
#define TAGANALYSE_H

#include <QDialog>
#include <QStringList>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QFont>
#include <QList>
#include <QDebug>
#include <QProgressBar>


namespace Ui {
class TagAnalyse;
}

class TagAnalyse : public QDialog
{
    Q_OBJECT
    
public:
    explicit TagAnalyse(QWidget *parent = 0);
    ~TagAnalyse();
    
public:
    void transParams(QStringList tagList,QStringList numList);   //向该对话框传入参数
    void show();        //开始显示分析后的图
    void updateDisp(QStringList tagList,QStringList numList);    //更新数据显示
    
private:
    void updateLayout(QStringList tagList,QStringList numList); //更新显示
    void getOrderList();    //获取按数量排序后的数据表
    
private slots:
    void orderASCIISlot();      //按ASCII码排序并显示
    void orderNumSlot();        //按数量排序并显示
    
private:
    Ui::TagAnalyse *ui;
    
    QWidget *mWidget;           //全局布局控件容器
    QStringList tagList;        //Tag表
    QStringList numList;        //数量表
    QStringList orderTagList;   //按数量排序后的Tag表
    QStringList orderNumList;   //按数量排序后的数量表
    int totalNum;               //总Tag数
    
};

#endif // TAGANALYSE_H
