#include "taganalyse.h"
#include "ui_taganalyse.h"


TagAnalyse::TagAnalyse(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TagAnalyse)
{
    ui->setupUi(this);
    mWidget = new QWidget;
    
    QObject::connect(ui->rbASCIIOrder,SIGNAL(toggled(bool)),this,SLOT(orderASCIISlot()));
    QObject::connect(ui->rbNumOrder,SIGNAL(toggled(bool)),this,SLOT(orderNumSlot()));
    
}


TagAnalyse::~TagAnalyse()
{
    delete ui;
}


/*
 * 函数名称：    transParams(QStringList tagList, QStringList numList)
 * 函数版本：        1.0.0
 * 作者：            HXL
 * 创建日期：    2017.5.4
 * 函数功能：    向该对话框传入参数
 * 输入参数：    tagList:Tag表    numList:数量表
 * 输出参数：    无
 * 返回值：      无
*/
void TagAnalyse::transParams(QStringList tagList, QStringList numList)
{
    this->tagList = tagList;
    this->numList = numList;
    
    if (tagList.size() != numList.size()) {
        QMessageBox::information(this,"警告","传入的Tag参数错误，请检查！");
        this->close();
    }
    
    int size = numList.size();
    totalNum = 0;
    for (int i = 0; i < size; ++i) {
        totalNum += numList.at(i).toInt();
    }
    
    getOrderList();     //获取按数量排序后的数据表
}


/*
 * 函数名称：    show()
 * 函数版本：        1.0.0
 * 作者：            HXL
 * 创建日期：    2017.5.4
 * 函数功能：    开始显示分析后的图
 * 输入参数：    无
 * 输出参数：    无
 * 返回值：      无
*/
void TagAnalyse::show()
{
    ui->labelTotal->setText("总Tag数：" + QString::number(totalNum));
    updateLayout(orderTagList,orderNumList);
}


/*
 * 函数名称：    updateDisp(QStringList tagList, QStringList numList)
 * 函数版本：        1.0.0
 * 作者：            HXL
 * 创建日期：    2017.5.4
 * 函数功能：    更新数据显示
 * 输入参数：    tagList:Tag表    numList:数量表
 * 输出参数：    无
 * 返回值：      无
*/
void TagAnalyse::updateDisp(QStringList tagList, QStringList numList)
{
    if (tagList.size() != numList.size()) {
        return;
    }
    
    this->tagList = tagList;
    this->numList = numList;
    
    int size = numList.size();
    totalNum = 0;
    for (int i = 0; i < size; ++i) {
        totalNum += numList.at(i).toInt();
    }
    
    getOrderList();     //获取按数量排序后的数据表
    
    ui->labelTotal->setText("总Tag数：" + QString::number(totalNum));
    if (ui->rbASCIIOrder->isChecked()) {
        updateLayout(tagList,numList);
    } else {
        updateLayout(orderTagList,orderNumList);
    }
    
}


/*
 * 函数名称：    orderASCIISlot()
 * 函数版本：        1.0.0
 * 作者：            HXL
 * 创建日期：    2017.5.4
 * 函数功能：    按ASCII码排序并显示
 * 输入参数：    无
 * 输出参数：    无
 * 返回值：      无
*/
void TagAnalyse::orderASCIISlot()
{
    if (ui->rbASCIIOrder->isChecked()) {
        updateLayout(tagList,numList);
    }
}


/*
 * 函数名称：    orderNumSlot()
 * 函数版本：        1.0.0
 * 作者：            HXL
 * 创建日期：    2017.5.4
 * 函数功能：    按数量排序并显示
 * 输入参数：    无
 * 输出参数：    无
 * 返回值：      无
*/
void TagAnalyse::orderNumSlot()
{
    if (ui->rbNumOrder->isChecked()) {
        updateLayout(orderTagList,orderNumList);
    }
}


/*
 * 函数名称：    transParams(QStringList tagList, QStringList numList)
 * 函数版本：        1.0.0
 * 作者：            HXL
 * 创建日期：    2017.5.4
 * 函数功能：    更新显示
 * 输入参数：    tagList:Tag表    numList:数量表
 * 输出参数：    无
 * 返回值：      无
*/
void TagAnalyse::updateLayout(QStringList tagList, QStringList numList)
{
    //先删除先前的控件
    ui->contentLayout->removeWidget(mWidget);
    delete mWidget;
    mWidget = new QWidget;
    
    QFont font = QFont("Courier New",12,QFont::Normal);
    QVBoxLayout *vLayout = new QVBoxLayout;
    QHBoxLayout *hLayout = new QHBoxLayout;
    QString text;
    int size = tagList.size();
    
    //开始布局
    int percent = 0;
    int curStep = 0;
    QString integerPart;
    QString decimalPart;
    for (int i = 0; i < size; ++i) {
        curStep = numList.at(i).toInt();
        
        hLayout = new QHBoxLayout;
        text = tagList.at(i);
        text = (text.isEmpty()) ? "null" : text;
        QLabel *label1 = new QLabel(text);
        label1->setFont(font);
        label1->setFixedSize(QSize(150,20));
        label1->setAlignment(Qt::AlignLeft);
        label1->setToolTip(text);
        
        QProgressBar *progress = new QProgressBar;
        progress->setFont(font);
        progress->setTextVisible(false);
        progress->setMinimum(0);
        progress->setMaximum(totalNum);
        progress->setFixedSize(QSize(400,20));
        progress->setValue(curStep);
        
        percent = (int)(curStep * 10000.0 / totalNum + 0.5);
        integerPart = QString::number(percent / 100);
        int temp = percent % 100;
        if (temp < 10) {
            decimalPart = "0" + QString::number(percent % 100);
        } else {
            decimalPart = QString::number(percent % 100);
        }
        QLabel *label2 = new QLabel(integerPart + "." + decimalPart + "%");
        label2->setFont(font);
        label2->setFixedSize(QSize(80,20));
        label2->setAlignment(Qt::AlignCenter);
        
        QLabel *label3 = new QLabel("(" + numList.at(i) + ")");
        label3->setFont(font);
        label3->setFixedHeight(20);
        label3->setAlignment(Qt::AlignLeft);
        
        hLayout->addWidget(label1);
        hLayout->addWidget(progress);
        hLayout->addWidget(label2);
        hLayout->addWidget(label3);
        
        vLayout->addLayout(hLayout);
    }
    vLayout->addStretch();
    
    mWidget->setLayout(vLayout);
    ui->contentLayout->addWidget(mWidget);
}


/*
 * 函数名称：    getOrderList()
 * 函数版本：        1.0.0
 * 作者：            HXL
 * 创建日期：    2017.5.4
 * 函数功能：    获取按数量排序后的数据表
 * 输入参数：    无
 * 输出参数：    无
 * 返回值：      无
*/
void TagAnalyse::getOrderList()
{
    int size = tagList.size();
    int num1;
    int num2;
    QString tag1;
    QString tag2;
    orderTagList = tagList;
    orderNumList = numList;
    
    //冒泡排序法
    for (int i = 0; i < (size - 1); ++i) {
        for (int j = 0; j < (size - 1 - i); ++j) {
            num1 = orderNumList.at(j).toInt();
            num2 = orderNumList.at(j + 1).toInt();
            if (num1 < num2) {
                orderNumList.replace(j,QString::number(num2));
                orderNumList.replace(j + 1,QString::number(num1));
                
                tag1 = orderTagList.at(j);
                tag2 = orderTagList.at(j + 1);
                orderTagList.replace(j,tag2);
                orderTagList.replace(j + 1,tag1);
            }
        }
    }
}




